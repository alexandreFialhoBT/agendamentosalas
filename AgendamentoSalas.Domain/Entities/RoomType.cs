﻿namespace AgendamentoSalas.Domain.Entities
{
    public class RoomType
    {
        public int RoomTypeId { get; set; }
        public string Name { get; set; }
    }
}
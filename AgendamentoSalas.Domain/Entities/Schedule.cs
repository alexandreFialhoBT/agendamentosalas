﻿using System;

namespace AgendamentoSalas.Domain.Entities
{
    public class Schedule
    {
        public int ScheduleId { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
    }
}
﻿namespace AgendamentoSalas.Domain.Entities
{
    public class RoomStatus
    {
        public int RoomStatusId { get; set; }
        public string Name { get; set; }
    }
}
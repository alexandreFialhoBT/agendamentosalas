﻿using System.Collections.Generic;

namespace AgendamentoSalas.Domain.Entities
{
    public class Room
    {
        public int RoomId { get; set; }
        public int BranchId { get; set; }
        public string Name { get; set; }
        public string RoomPicture { get; set; }
        public int Capacity { get; set; }
        public int RoomStatusId { get; set; }
        public int ScheduleId { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual RoomType RoomType { get; set; }
        public virtual RoomStatus Status { get; set; }
        public virtual Schedule Schedule { get; set; }
        public virtual List<Equipment> Equipments { get; set; }
        
    }
}

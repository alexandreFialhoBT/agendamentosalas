﻿namespace AgendamentoSalas.Domain.Entities
{
    public class Branch
    {
        public int BranchId { get; set; }
        public int StateId { get; set; }
        public string Name { get; set; }
        public virtual State State { get; set; }
    }
}
﻿namespace AgendamentoSalas.Domain.Entities
{
    public class Equipment
    {
        public int EquipmentId { get; set; }
        public string Name { get; set; }
    }
}